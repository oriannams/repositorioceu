package aplicacion; 
import java.util.Scanner;
import java.util.InputMismatchException;

public class Agenda{
	public static void main(String args[]){
		boolean condicion = true;
		Scanner sc = new Scanner(System.in);
	    while(condicion){	
		try{
			System.out.println("Introduzca el numero que esta buscando:");
			int dia = sc.nextInt();
			condicion = false;
			         
			switch(dia){
				case 1:
					System.out.println("El dia 1 es Lunes");
					break;
				case 2:
        	                        System.out.println("El dia 2 es martes");
                	                break;
				case 3:
        	                        System.out.println("El dia 3 es miercoles");
                	                break;
                        	case 4:
                                	System.out.println("El dia 4 es jueves");
	                                break;
				case 5:
                	                System.out.println("El dia 5 es viernes");
                        	        break;
	                        case 6:
        	                        System.out.println("El dia 6 es sabado");
	                                break;
        	                case 7:
                	                System.out.println("El dia 7 es domingo");
                        	        break;
				default:
					System.out.println("El dia que selecciono no esta comprendido entre los de la semana.");
					condicion = true;
		}
	

		}catch(InputMismatchException e){
			sc.next();
			System.out.println("Lo siento su argumento no es valido");
		}
	
	    }
	    sc.close();
	}

}

