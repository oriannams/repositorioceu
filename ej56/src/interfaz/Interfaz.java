package interfaz;

import dominio.*;
import java.lang.ArrayIndexOutOfBoundsException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Interfaz{
	static Libreta libreta = inicializarLibreta();
	private static String NOMBRE_FICHERO = "libreta.txt";

	public static void procesarPeticion(String sentencia){
		String[] palabras = sentencia.split(" ");
		if (palabras[0].equalsIgnoreCase("add") && palabras.length == 3){
			Contacto contacto = new Contacto(palabras[1], palabras[2]);
			libreta.annadirContacto(contacto);
			System.out.println(libreta);
			guardarLibreta(libreta);
		}else if (palabras[0].equalsIgnoreCase("list")){
			System.out.println(libreta);
		}else if (palabras[0].equalsIgnoreCase("help")){
			printHelp();
		}else {
			System.out.println("Lo siento, operacion no valida.");
		}
	}

	private static void guardarLibreta(Libreta libreta){
			//Para escribir en ficheros
		try{
			FileWriter fw = new FileWriter(NOMBRE_FICHERO);
			fw.write(libreta.toString());
			fw.close();
			System.out.println("Se ha guardado el archivo");
		}catch(IOException e){
		//
		}

	}

	private static Libreta inicializarLibreta(){
		Libreta libreta = new Libreta();
		try{
			//Para leer en ficheros
			File file = new File(NOMBRE_FICHERO);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
			//String nombre = sc.next();
			//String telefono = sc.next();
			//Contacto contacto = new Contacto(nombre, telefono);
			COntacto contacto = new Contacto(sc.next(), sc.next());
			libreta.annadirContacto(contacto);
		}
		sc.close();
		}catch(FileNotFoundException e){
		//
		}
		return libreta;
	}


	private static void printHelp(){
		String ayuda = "Las operaciones posibles son las siguientes:\n- Añadir contacto: ‘java -jar libreta.jar add <nombre> <teléfono>‘\nPor ejemplo,\njava -jar libreta.jar add Pepe 654321234\n- Mostrar contactos:\njava -jar libreta.jar list\n- Mostrar esta ayuda:\njava -jar libreta.jar help";
		System.out.println(ayuda);
	}

}
