package aplicacion;

import dominio.*;
import interfaz.*;

public class Principal{
        public static void main(String args[]){
		Interfaz solicitud = new Interfaz();
		String sentencia = "";
		for (int i = 0; i < args.length; i++){
			sentencia += args[i];	
		}
		solicitud.procesarPeticion(sentencia);

}
}
//Con leer los caracteres de los argumentos, puedo ponerlo a identificar la primera palabra. 
