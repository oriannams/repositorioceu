package aplicacion; 
import dominio.*;
import java.util.ArrayList;


public class Bachillerato{
	public static void main(String args[]){
		Asignatura math = new Asignatura();
		Asignatura cast = new Asignatura();
		math.setNombre("Matematicas");
		cast.setNombre("Castellano");

		Curso primero = new Curso();
		primero.setNombre("Primer año");

		primero.addAsignatura(math);
		primero.addAsignatura(cast);

		System.out.println(primero);
		
	}
}

