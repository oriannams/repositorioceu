package dominio; 
import java.util.ArrayList;


public class Curso{
	private String nombre;
	private ArrayList<Asignatura> asignaturas = new ArrayList<>();


	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public String getNombre(){
		return nombre;
	}

	public ArrayList<Asignatura> getAsignaturas(){
		return asignaturas;
	}
	public void setAsignaturas(ArrayList<Asignatura> asignaturas){
		this.asignaturas = asignaturas;
	}

	//Me perdi un poco, porque es addAsignatura, en singular, no deberias agregarlo 
	//al objeto de arraylist que se llama asignaturas.
	public void addAsignatura(Asignatura asignatura){
		asignaturas.add(asignatura);
	}

	public String toString(){
		String mensaje = "El curso " + nombre +" tiene estas asignaturas:\n";
		for(int i = 0; i < asignaturas.size(); i++){
			mensaje += asignaturas.get(i) + "\n";
		}
		return mensaje;
	
		}
}
