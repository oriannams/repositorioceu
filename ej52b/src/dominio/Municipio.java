package dominio;
import java.util.ArrayList;
import dominio.*;
public class Municipio{
	private String nombre;
	private ArrayList<Localidad> localidades = new ArrayList();

	public void setLocalidades(ArrayList<Localidad> localidades){
		this.localidades = localidades;
	}
	public ArrayList<Localidad> getLocalidades(){
		return localidades;
	}
	public void addLocalidad(Localidad localid2){
		localidades.add(localid2);
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getNombre(){
		return nombre;
	}
	
	public int calcHabit(){
		int habitantesMun = 0;
		for(int i = 0; i < localidades.size(); i++){
			habitantesMun += localidades.get(i).getNumeroDeHabitantes();
		}
		return habitantesMun;
	}
	
	public String toString(){
		String mensaje = nombre + ", con un total de " + calcHabit() + " habitantes. Conformada por las localidades de: \n";
		for(int i = 0; i < localidades.size(); i++){
			mensaje += localidades.get(i);
		}
		return mensaje;

	}



}
