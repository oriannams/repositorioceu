package dominio;
//import aplicacion.*;


public class Localidad{

	private String nombre;
	private int numeroDeHabitantes; 

	public Localidad(String nombre, int numeroDeHabitantes){
		this.nombre = nombre;
		this.numeroDeHabitantes = numeroDeHabitantes;
	}

	public Localidad(){
		nombre = "";
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getNombre(){
		return nombre;
	}

	public void setNumeroDeHabitantes(int numeroDeHabitantes){
		this.numeroDeHabitantes = numeroDeHabitantes;
	}
	public int getNumeroDeHabitantes(){
		return numeroDeHabitantes;
	}
	
	public String toString(){
		return nombre + " con " + numeroDeHabitantes + " habitantes.\n";
	}





}
