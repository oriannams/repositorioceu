package dominio;

public class Clase{
	private String nombre;
	private int numeroAlumnos;
	private String direccion;

	public Clase(String nombre, int numeroAlumnos){
		System.out.println("Se ha creado un obj de la clase.");
			this.nombre = nombre; 
			this.numeroAlumnos = numeroAlumnos;
	}
	public Clase(){
	}

	public Clase(int numeroAlumnos){
		nombre = "";
		this.numeroAlumnos = numeroAlumnos;
	}
	public Clase(String nombre, String direccion){
		this.direccion = direccion;
	}


	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public int getNumeroAlumnos(){
		return numeroAlumnos;
	}
	public void setNumeroAlumnos(int numeroAlumnos){
		this.numeroAlumnos = numeroAlumnos;
	}


	public String toString(){
 		return "La clase se llama " + nombre + " y tiene "+ numeroAlumnos + " alumnos"; }

}
