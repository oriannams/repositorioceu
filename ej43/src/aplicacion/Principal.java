package aplicacion;
import dominio.*;

public class Principal{
        public static void main(String args[]){
		Localidad laCruz = new Localidad();
		laCruz.setNombre("La Cruz");
		laCruz.setNumeroDeHabitantes(2000);
		
		Localidad floresta = new Localidad();
		floresta.setNombre("La Floresta");
		floresta.setNumeroDeHabitantes(1500);

		Localidad tadeo = new Localidad();
		tadeo.setNombre("Jose Tadeo");
		tadeo.setNumeroDeHabitantes(800);
	

		Municipio maturin = new Municipio();
		maturin.setNombre("Maturin");
		maturin.addLocalidad(laCruz);
		maturin.addLocalidad(floresta);

		Municipio puntaDeMata = new Municipio();
		puntaDeMata.setNombre("Punta de Mata");
		puntaDeMata.addLocalidad(tadeo);

		Provincia monagas = new Provincia();
		monagas.setNombre("Monagas:");
		monagas.addMunicipio(maturin);
		monagas.addMunicipio(puntaDeMata);

//		System.out.println(laCruz);
//		System.out.println(floresta);
//		System.out.println(tadeo);
//		System.out.println("Division 1");
//		System.out.println(puntaDeMata);
//		System.out.println(maturin);
//		System.out.println("Division 2");
		System.out.println(monagas);

	}
}

