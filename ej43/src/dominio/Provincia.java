package dominio;
//import dominio.*;
import java.util.ArrayList;

public class Provincia{

	private String nombre;
        private ArrayList<Municipio> municipios = new ArrayList();
	
	public void setMunicipios(ArrayList<Municipio> municipios){
		this.municipios = municipios;
	}	
	public ArrayList<Municipio> getMunicipios(){	
		return municipios;
	}
	public void addMunicipio(Municipio municip){
		municipios.add(municip);
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getNombre(){
		return nombre;
	}
	public int calcHabit(){
		int habitantesProv = 0;
		for(int i = 0; i < municipios.size(); i++){
			habitantesProv += municipios.get(i).calcHabit();
		}
		return habitantesProv;
	}
	public String toString(){
		String mensaje = "La provincia " + nombre + " cuenta con "+ calcHabit() + " habitantes. Conformada por los municipios: \n";
		for(int i = 0; i < municipios.size(); i++){
			mensaje += ">>" + municipios.get(i) + "\n";
		}
		return mensaje;
	}

}
