package dominio;
//import aplicacion.*;


public class Localidad{
//	public static void main(){
	private String nombre;
	private int numeroDeHabitantes; 

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getNombre(){
		return nombre;
	}

	public void setNumeroDeHabitantes(int numeroDeHabitantes){
		this.numeroDeHabitantes = numeroDeHabitantes;
	}
	public int getNumeroDeHabitantes(){
		return numeroDeHabitantes;
	}
	
	public String toString(){
		return nombre + " con " + numeroDeHabitantes + " habitantes.\n";
	}




//}
}
