package aplicacion;
//import dominio.*;

public class Principal{
        public static void main(String args[]){
		//if (args.length == 0 && args.length){...}  | que representa un or. || representa un or en cascada. es decir, que se cumpla uno, ya le es suficiente, para los || o los &&.
	   //if(args.length){alternativa a la excepcion}
	   try{
		int resultado = args[0].compareTo(args[1]);		    
		//if(args[0].equals(args[1])){...} Devuelve un valor de tipo booleano.
		if (resultado == 0){
			System.out.println("Los parametros son iguales");
		} else {
			System.out.println("Los parametros son diferentes");}
		if(args[0].equalsIgnoreCase(args[1])){
			System.out.println("Son iguales sin importar las may/mis");}
	   }catch(Exception e){
		   System.out.println("Agrega dos parametros");
	}

	}
}

